Author: Alex Vischer
Contact Address: 2428 NE Alameda St.
Email: avischer@uoregon.edu

This program is designed to say "Hello World" at the press of a button. It is truly a hallmark software
design.